# Gaelic; Scottish translation for camera-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the camera-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
# GunChleoc <fios@foramnagaidhlig.net>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: camera-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-23 17:57+0000\n"
"PO-Revision-Date: 2019-08-22 18:49+0000\n"
"Last-Translator: Michael Bauer <teanga@igaidhlig.net>\n"
"Language-Team: Gaelic <https://translate.ubports.com/projects/ubports/"
"camera-app/gd/>\n"
"Language: gd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 || n==11) ? 0 : (n==2 || n==12) ? 1 : "
"(n > 2 && n < 20) ? 2 : 3;\n"
"X-Generator: Weblate 3.6.1\n"
"X-Launchpad-Export-Date: 2016-12-01 04:53+0000\n"

#: ../AdvancedOptions.qml:16
msgid "Settings"
msgstr "Roghainnean"

#: ../AdvancedOptions.qml:48
msgid "Add date stamp on captured images"
msgstr ""

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:70
msgid "Format"
msgstr ""

#: ../AdvancedOptions.qml:122
msgid "Date formatting keywords"
msgstr ""

#: ../AdvancedOptions.qml:127
msgid "the day as number without a leading zero (1 to 31)"
msgstr ""

#: ../AdvancedOptions.qml:128
msgid "the day as number with a leading zero (01 to 31)"
msgstr ""

#: ../AdvancedOptions.qml:129
msgid "the abbreviated localized day name (e.g. 'Mon' to 'Sun')."
msgstr ""

#: ../AdvancedOptions.qml:130
msgid "the long localized day name (e.g. 'Monday' to 'Sunday')."
msgstr ""

#: ../AdvancedOptions.qml:131
msgid "the month as number without a leading zero (1 to 12)"
msgstr ""

#: ../AdvancedOptions.qml:132
msgid "the month as number with a leading zero (01 to 12)"
msgstr ""

#: ../AdvancedOptions.qml:133
msgid "the abbreviated localized month name (e.g. 'Jan' to 'Dec')."
msgstr ""

#: ../AdvancedOptions.qml:134
msgid "the long localized month name (e.g. 'January' to 'December')."
msgstr ""

#: ../AdvancedOptions.qml:135
msgid "the year as two digit number (00 to 99)"
msgstr ""

#: ../AdvancedOptions.qml:136
msgid ""
"the year as four digit number. If the year is negative, a minus sign is "
"prepended in addition."
msgstr ""

#: ../AdvancedOptions.qml:137
msgid "the hour without a leading zero (0 to 23 or 1 to 12 if AM/PM display)"
msgstr ""

#: ../AdvancedOptions.qml:138
msgid "the hour with a leading zero (00 to 23 or 01 to 12 if AM/PM display)"
msgstr ""

#: ../AdvancedOptions.qml:139
msgid "the hour without a leading zero (0 to 23, even with AM/PM display)"
msgstr ""

#: ../AdvancedOptions.qml:140
msgid "the hour with a leading zero (00 to 23, even with AM/PM display)"
msgstr ""

#: ../AdvancedOptions.qml:141
msgid "the minute without a leading zero (0 to 59)"
msgstr ""

#: ../AdvancedOptions.qml:142
msgid "the minute with a leading zero (00 to 59)"
msgstr ""

#: ../AdvancedOptions.qml:143
msgid "the second without a leading zero (0 to 59)"
msgstr ""

#: ../AdvancedOptions.qml:144
msgid "the second with a leading zero (00 to 59)"
msgstr ""

#: ../AdvancedOptions.qml:145
msgid "the milliseconds without leading zeroes (0 to 999)"
msgstr ""

#: ../AdvancedOptions.qml:146
msgid "the milliseconds with leading zeroes (000 to 999)"
msgstr ""

#: ../AdvancedOptions.qml:147
msgid "use AM/PM display. AP will be replaced by either 'AM' or 'PM'."
msgstr ""

#: ../AdvancedOptions.qml:148
msgid "use am/pm display. ap will be replaced by either 'am' or 'pm'."
msgstr ""

#: ../AdvancedOptions.qml:149
msgid "the timezone (for example 'CEST')"
msgstr ""

#: ../AdvancedOptions.qml:158
msgid "Add to Format"
msgstr ""

#. TRANSLATORS: this refers to the color of date stamp added to captured images
#: ../AdvancedOptions.qml:189
msgid "Color"
msgstr ""

#. TRANSLATORS: this refers to the alignment of date stamp within captured images (bottom left, top right,etc..)
#: ../AdvancedOptions.qml:257
msgid "Alignment"
msgstr ""

#. TRANSLATORS: this refers to the opacity  of date stamp added to captured images
#: ../AdvancedOptions.qml:303
msgid "Opacity"
msgstr ""

#: ../DeleteDialog.qml:24
msgid "Delete media?"
msgstr "A bheil thu airson am meadhan a sguabadh às?"

#: ../DeleteDialog.qml:34 ../PhotogridView.qml:56 ../SlideshowView.qml:75
msgid "Delete"
msgstr "Sguab às"

#: ../DeleteDialog.qml:40 ../ViewFinderOverlay.qml:1122
#: ../ViewFinderOverlay.qml:1136 ../ViewFinderOverlay.qml:1175
#: ../ViewFinderView.qml:494
msgid "Cancel"
msgstr "Sguir dheth"

#: ../GalleryView.qml:200
msgid "No media available."
msgstr "Chan eil meadhan ri làimh."

#: ../GalleryView.qml:235
msgid "Scanning for content..."
msgstr "A' sganadh airson susbaint..."

#: ../GalleryViewHeader.qml:82 ../SlideshowView.qml:45
msgid "Select"
msgstr "Tagh"

#: ../GalleryViewHeader.qml:83
msgid "Edit Photo"
msgstr "Deasaich an dealbh"

#: ../GalleryViewHeader.qml:83
msgid "Photo Roll"
msgstr "Albam a' chamara"

#: ../Information.qml:20
msgid "About"
msgstr ""

#: ../Information.qml:82
msgid "Get the source"
msgstr ""

#: ../Information.qml:83
msgid "Report issues"
msgstr ""

#: ../Information.qml:84
msgid "Help translate"
msgstr ""

#: ../MediaInfoPopover.qml:25
msgid "Media Information"
msgstr ""

#: ../MediaInfoPopover.qml:30
#, qt-format
msgid "Name : %1"
msgstr ""

#: ../MediaInfoPopover.qml:33
#, qt-format
msgid "Type : %1"
msgstr ""

#: ../MediaInfoPopover.qml:37
#, qt-format
msgid "Width : %1"
msgstr ""

#: ../MediaInfoPopover.qml:41
#, qt-format
msgid "Height : %1"
msgstr ""

#: ../NoSpaceHint.qml:33
msgid "No space left on device, free up space to continue."
msgstr ""
"Chan eil àite air fhàgail air an uidheam, saor àite mus lean thu air adhart."

#: ../PhotoRollHint.qml:69
msgid "Swipe left for photo roll"
msgstr "Grad-shlaighd gu clì airson albam nan dealbhan"

#: ../PhotogridView.qml:42 ../SlideshowView.qml:60
msgid "Share"
msgstr "Co-roinn"

#: ../PhotogridView.qml:69 ../SlideshowView.qml:53
msgid "Gallery"
msgstr ""

#: ../SlideshowView.qml:68
msgid "Image Info"
msgstr ""

#: ../SlideshowView.qml:86
msgid "Edit"
msgstr "Deasaich"

#: ../SlideshowView.qml:441
#, fuzzy
msgid "Back to Photo roll"
msgstr "Albam a' chamara"

#: ../UnableShareDialog.qml:25
msgid "Unable to share"
msgstr ""

#: ../UnableShareDialog.qml:26
msgid "Unable to share photos and videos at the same time"
msgstr ""

#: ../UnableShareDialog.qml:30
msgid "Ok"
msgstr ""

#: ../ViewFinderOverlay.qml:367 ../ViewFinderOverlay.qml:390
#: ../ViewFinderOverlay.qml:418 ../ViewFinderOverlay.qml:441
#: ../ViewFinderOverlay.qml:526 ../ViewFinderOverlay.qml:589
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:42
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:65
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:86
msgid "On"
msgstr "Air"

#: ../ViewFinderOverlay.qml:372 ../ViewFinderOverlay.qml:400
#: ../ViewFinderOverlay.qml:423 ../ViewFinderOverlay.qml:446
#: ../ViewFinderOverlay.qml:465 ../ViewFinderOverlay.qml:531
#: ../ViewFinderOverlay.qml:599
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:47
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:70
#: ../tests/unittests/tst_BottomEdgeIndicators.qml:91
msgid "Off"
msgstr "Dheth"

#: ../ViewFinderOverlay.qml:395
msgid "Auto"
msgstr "Fèin-obrachail"

#: ../ViewFinderOverlay.qml:432
msgid "HDR"
msgstr "HDR"

#: ../ViewFinderOverlay.qml:470
msgid "5 seconds"
msgstr "5 diogan"

#: ../ViewFinderOverlay.qml:475
msgid "15 seconds"
msgstr "15 diogan"

#: ../ViewFinderOverlay.qml:493
msgid "Fine Quality"
msgstr "Càileachd àrd"

#: ../ViewFinderOverlay.qml:498
#, fuzzy
msgid "High Quality"
msgstr "Càileachd àrd"

#: ../ViewFinderOverlay.qml:503
msgid "Normal Quality"
msgstr "Càileachd àbhaisteach"

#: ../ViewFinderOverlay.qml:508
msgid "Basic Quality"
msgstr "Càileachd bhunasach"

#. TRANSLATORS: this will be displayed on an small button so for it to fit it should be less then 3 characters long.
#: ../ViewFinderOverlay.qml:541
msgid "SD"
msgstr "SD"

#: ../ViewFinderOverlay.qml:549
msgid "Save to SD Card"
msgstr "Sàbhail air a' chairt SD"

#: ../ViewFinderOverlay.qml:554
msgid "Save internally"
msgstr "Sàbhail air an uidheam"

#: ../ViewFinderOverlay.qml:594
msgid "Vibrate"
msgstr ""

#: ../ViewFinderOverlay.qml:1119
msgid "Low storage space"
msgstr "Tha an t-àite a' fàs gann"

#: ../ViewFinderOverlay.qml:1120
msgid ""
"You are running out of storage space. To continue without interruptions, "
"free up storage space now."
msgstr ""
"Tha an rum air an stòras gu bhith ruith ort. Saor rum an-dràsta ach an lean "
"thu air adhart gun stad."

#: ../ViewFinderOverlay.qml:1133
msgid "External storage not writeable"
msgstr ""

#: ../ViewFinderOverlay.qml:1134
msgid ""
"It does not seem possible to write to your external storage media. Trying to "
"eject and insert it again might solve the issue, or you might need to format "
"it."
msgstr ""

#: ../ViewFinderOverlay.qml:1172
msgid "Cannot access camera"
msgstr "Chan fhaigh sinn cothrom air a’ chamara"

#: ../ViewFinderOverlay.qml:1173
#, fuzzy
msgid ""
"Camera app doesn't have permission to access the camera hardware or another "
"error occurred.\n"
"\n"
"If granting permission does not resolve this problem, reboot your device."
msgstr ""
"Chan eil cead aig aplacaid a’ chamara air bathar-cruaidh a’ chamara no "
"thachair mearachd eile.\n"
"\n"
"Ma bheir thu cead dha ’s mur an càraich sin an duilgheadas, ath-thòisich am "
"fòn agad."

#: ../ViewFinderOverlay.qml:1182
msgid "Edit Permissions"
msgstr "Deasaich na ceadan"

#: ../ViewFinderView.qml:485
msgid "Capture failed"
msgstr ""

#: ../ViewFinderView.qml:490
msgid ""
"Replacing your external media, formatting it, or restarting the device might "
"fix the problem."
msgstr ""

#: ../ViewFinderView.qml:491
msgid "Restarting your device might fix the problem."
msgstr ""

#: ../camera-app.qml:57
msgid "Flash"
msgstr "Solas-boillsgidh"

#: ../camera-app.qml:58
msgid "Light;Dark"
msgstr "Light;Dark;sorcha;dorcha"

#: ../camera-app.qml:61
msgid "Flip Camera"
msgstr "Thoir flip dhen chamara"

#: ../camera-app.qml:62
msgid "Front Facing;Back Facing"
msgstr "Front Facing;Back Facing;an comhair a bheòil;an comhair a chùil"

#: ../camera-app.qml:65
msgid "Shutter"
msgstr "Siutair"

#: ../camera-app.qml:66
msgid "Take a Photo;Snap;Record"
msgstr "Take a Photo;Snap;Record;tog dealbh;greimich;clàraich"

#: ../camera-app.qml:69
msgid "Mode"
msgstr "Modh"

#: ../camera-app.qml:70
msgid "Stills;Video"
msgstr "Stills;Video"

#: ../camera-app.qml:74
msgid "White Balance"
msgstr "Am balans geal"

#: ../camera-app.qml:75
msgid "Lighting Condition;Day;Cloudy;Inside"
msgstr ""
"Lighting Condition;Day;Cloudy;Inside;dè solas;latha;neulach;sgòthach;a-staigh"

#: ../camera-app.qml:380
#, qt-format
msgid "<b>%1</b> photos taken today"
msgstr "<b>%1</b> dealbh(an) air an togail an-diugh"

#: ../camera-app.qml:381
msgid "No photos taken today"
msgstr "Cha deach dealbh sam bith a thogail an-diugh"

#: ../camera-app.qml:391
#, qt-format
msgid "<b>%1</b> videos recorded today"
msgstr "<b>%1</b> video air a clàradh an-diugh"

#: ../camera-app.qml:392
msgid "No videos recorded today"
msgstr "Cha deach video sam bith a chlàradh an-diugh"

#: camera-app.desktop.in.in.h:1
msgid "Camera"
msgstr "Camara"

#: camera-app.desktop.in.in.h:2
msgid "Camera application"
msgstr "Aplacaid a' chamara"

#: camera-app.desktop.in.in.h:3
msgid "Photos;Videos;Capture;Shoot;Snapshot;Record"
msgstr ""
"Photos;Videos;Capture;Shoot;Snapshot;Record;Dealbhan;Dealbhan-camara;Deilbh;"
"Videothan;Glacadh;Togail;Clàradh"
